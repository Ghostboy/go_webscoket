# go_webscoket

1. **WebScoket协议分析：**

​	WebSocket 协议解决了浏览器和服务器之间的全双工通信问题。在 WebSocket 出现之前，浏览器如果需要从服务器及时获得更新，则需要不停的对服务器主动发起请求，也就是 Web 中常用的`poll`技术。这样的操作非常低效，这是因为每发起一次新的 HTTP 请求，就需要单独开启一个新的 TCP 链接，同时 HTTP 协议本身也是一种开销非常大的协议。为了解决这些问题，所以出现了 WebSocket 协议。WebSocket 使得浏览器和服务器之间能通过一个持久的 TCP 链接就能完成数据的双向通信。

​	WebSocket 和 HTTP 协议一般情况下都工作在浏览器中，但 WebSocket 是一种完全不同于 HTTP 的协议。尽管，浏览器需要通过 HTTP 协议的`GET`请求，将 HTTP 协议升级为 WebSocket 协议。升级的过程被称为`握手(handshake)`。当浏览器和服务器成功握手后，则可以开始根据 WebSocket 定义的通信帧格式开始通信了。像其他各种协议一样，WebSocket 协议的通信帧也分为控制数据帧和普通数据帧，前者用于控制 WebSocket 链接状态，后者用于承载数据。

2. **WebScoket协议的握手过程：**

​	握手的过程也就是将 HTTP 协议升级为 WebSocket 协议的过程。握手开始首先由浏览器端发送一个`GET`请求开发，该请求的 HTTP 头部信息如下：

```http
GET /chat HTTP/1.1
Host: server.example.com
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Key: dGhlIHNhbXBsZSBub25jZQ==
Origin: http://example.com
Sec-WebSocket-Protocol: chat, superchat
Sec-WebSocket-Version: 13
```

当服务器端，成功验证了以上信息后，则会返回一个形如以下信息的响应:

```http
HTTP/1.1 101 Switching Protocols
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Accept: s3pPLMBiTxaQ9kYGzzhZRbK+xOo=
Sec-WebSocket-Protocol: chat
```

浏览器发送的 HTTP 请求中，增加了一些新的字段，其作用如下所示：

- `Upgrade`: 规定必需的字段，其值必需为 `websocket`, 如果不是则握手失败；
- `Connection`: 规定必需的字段，值必需为 `Upgrade`, 如果不是则握手失败；
- `Sec-WebSocket-Key`: 必需字段，一个随机的字符串；
- `Sec-WebSocket-Protocol`: 可选字段，可以用于标识应用层的协议；
- `Sec-WebSocket-Version`: 必需字段，代表了 WebSocket 协议版本，值必需是 `13`, 否则握手失败；

返回的响应中，如果握手成功会返回状态码为 `101` 的 HTTP 响应。同时其他字段说明如下：

- `Upgrade`: 规定必需的字段，其值必需为 `websocket`, 如果不是则握手失败；
- `Connection`: 规定必需的字段，值必需为 Upgrade, 如果不是则握手失败；
- `Sec-WebSocket-Accept`: 规定必需的字段，该字段的值是通过固定字符串`258EAFA5-E914-47DA-95CA-C5AB0DC85B11`加上请求中`Sec-WebSocket-Key`字段的值，然后再对其结果通过 SHA1 哈希算法求出的结果。
- `Sec-WebSocket-Protocol`: 对应于请求中的 `Sec-WebSocket-Protocol` 字段；

当浏览器和服务器端成功握手后，就可以传送数据了，传送数据是按照 WebSocket 协议的数据格式生成的。
