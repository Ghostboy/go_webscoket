package go_webscoket

import (
	"crypto/sha1"
	"encoding/base64"
	"net/http"
	"strings"
)

//对数据进行掩码处理
//将输出的数据按byte处理
func maskBytes(key [4]byte, pos int, b []byte) int {
	for i := range b {
		b[i] ^= key[pos&3]
		pos++
	}
	//按位要快一些，a % (2^n) 等价于 a & (2^n -1)
	return pos & 3 // pos % 4
}

// 检查http头部字段中是否包含指定的值
func tokenListContainsValue(header http.Header, name string, value string) bool {
	for _, v := range header[name] {
		for _, s := range strings.Split(v, ",") {
			if strings.EqualFold(value, strings.TrimSpace(s)) {
				return true
			}
		}
	}
	return false
}

var keyGUID = []byte("258EAFA5-E914-47DA-95CA-C5AB0DC85B11")

//计算websocket握手过程中生成的 Accept Key
func computeAcceptKey(challengeKey string) string {
	h := sha1.New()
	h.Write([]byte(challengeKey))
	h.Write(keyGUID)
	return base64.StdEncoding.EncodeToString(h.Sum(nil))

}
