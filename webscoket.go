package go_webscoket

import (
	"bufio"
	"encoding/binary"
	"errors"
	"log"
	"net"
	"net/http"
)

const (
	// 是否是最后一个数据帧, 8位1byte
	finallBit = 1 << 7
	// 是否需要进行掩码处理
	maskBit = 1 << 7

	// 文本类型数据帧
	TextMessage = 1
	// 关闭数据帧类型
	CloseMessage = 8
)

// WebScoket 链接
type Conn struct {
	writeBuf []byte
	// 表示用于解码的key 32bit
	maskKey [4]byte

	//底层的tcp链接，Conn是一个通用的面向流的网络连接
	conn net.Conn
}

func newConn(conn net.Conn) *Conn {
	return &Conn{conn: conn}
}

func (c *Conn) Close() {
	c.conn.Close()
}

// 发送数据，只发送文本数据
func (c *Conn) SendData(data []byte) {
	length := len(data)
	c.writeBuf = make([]byte, 10+length)

	// 数据开始和结束的位置
	payloadStart := 2

	// 数据帧的第一个字节，不支持分片，只能发送文本类型的数据
	// 其二进制位位 %b1000 0001
	c.writeBuf[0] = byte(TextMessage) | finallBit //b[0] = []byte{0x81}

	// 数据帧的第二个字节，服务器发送的数据不需要进行掩码处理
	switch {
	case length >= 65536:
		c.writeBuf[1] = byte(0x00) | 127
		//主机字节序（小端），网络字节序（大端）
		binary.BigEndian.PutUint64(c.writeBuf[payloadStart:], uint64(length))
		// 需要 8 byte 来存储数据长度
		payloadStart += 8
	case length > 125:
		c.writeBuf[1] = byte(0x00) | 126
		binary.BigEndian.PutUint16(c.writeBuf[payloadStart:], uint16(length))
		// 需要 2 byte 来存储数据长度
		payloadStart += 2
	default:
		c.writeBuf[1] = byte(0x00) | byte(length)
	}
	copy(c.writeBuf[payloadStart:], data[:])
	c.conn.Write(c.writeBuf[:payloadStart+length])
}

// 读取数据
func (c *Conn) ReadData() (data []byte, err error) {
	var b [8]byte
	// 读取数据帧的前两个字节
	if _, err := c.conn.Read(b[:2]); err != nil {
		return nil, err
	}

	// 开始解析第一个字节，是否还有后续的数据帧, 0 表示后面还有更多的数据帧，1 表示这是最后一个数据帧
	final := b[0]&finallBit != 0
	if !final {
		log.Println("not bit")
		return nil, errors.New("no message")
	}
	// 数据帧类型
	frameType := int(b[0] & 0xf)
	// 如果是关闭类型，则关闭链接
	if frameType == CloseMessage {
		c.conn.Close()
		log.Println("closed")
		return nil, errors.New("close message")
	}
	// 判断文本类型
	if frameType != TextMessage {
		return nil, errors.New("no text message")
	}
	// 0 表示数据帧没有经过掩码计算，1 表示数据帧经过掩码计算，一般情况下，只有浏览器发送给服务器的数据帧才需要进行掩码计算
	mask := b[1]&maskBit != 0

	// 数据长度
	payloadLen := int64(b[1] & 0x7F)
	dataLen := int64(payloadLen)
	// 根据payload length 判断数据的真实长度
	switch payloadLen {
	case 126:
		if _, err := c.conn.Read(b[:2]); err != nil {
			return nil, err
		}
		dataLen = int64(binary.BigEndian.Uint16(b[:2]))
	case 127:
		if _, err := c.conn.Read(b[:8]); err != nil {
			return nil, err
		}
		dataLen = int64(binary.BigEndian.Uint64(b[:8]))
	}

	// 读取mask key
	if mask {
		if _, err := c.conn.Read(c.maskKey[:]); err != nil {
			return nil, err
		}
	}

	// 读取内容
	p := make([]byte, dataLen)

	if _, err := c.conn.Read(p); err != nil {
		return nil, err
	}
	if mask {
		maskBytes(c.maskKey, 0, p)
	}

	return p, nil
}

// 将 http 链接升级到 webscoket 链接, 握手过程其实就是检查Http请求体头部字段的过程
func Upgrade(w http.ResponseWriter, r *http.Request) (c *Conn, err error) {
	// 检查是否是GET方法
	if r.Method != "GET" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return nil, errors.New("no GET")
	}
	// 检查Sec-WebSocket-Version 版本
	if values := r.Header["Sec-Websocker-Version"]; len(values) == 0 || values[0] != "13" {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return nil, errors.New("version != 13")
	}

	// 检查 Connection 和 Upgrade
	if !tokenListContainsValue(r.Header, "Connection", "Upgrade") {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return nil, errors.New("could find connection header with token 'upgrade'")
	}

	// 计算 Sec-WebSocket-Accept 的值
	challengeKey := r.Header.Get("Sec-Webscoket-ket")
	if challengeKey == "" {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return nil, errors.New("key missing")
	}

	var (
		netConn net.Conn
		br      *bufio.Reader
	)
	// Hijacker接口由ResponseWriters实现，它允许HTTP处理程序接管连接。
	//HTTP /1.x连接的默认ResponseWriter支持Hijacker，但HTTP /2连接有意不支持。 ResponseWriter包装器也可能不支持Hijacker。处理程序应始终在运行时测试此功能
	h, ok := w.(http.Hijacker)
	if !ok {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return nil, errors.New("response don't support hijack")
	}
	var rw *bufio.ReadWriter
	netConn, rw, err = h.Hijack()
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return nil, err
	}
	br = rw.Reader
	if br.Buffered() > 0 {
		netConn.Close()
		return nil, errors.New("client sent data before handshake is complete")
	}

	// 构造握手成功后返回 response

	p := []byte{}
	p = append(p, "HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Accept: "...)
	p = append(p, computeAcceptKey(challengeKey)...)
	p = append(p, "/r/n/r/n"...)

	if _, err = netConn.Write(p); err != nil {
		netConn.Close()
		return nil, err
	}
	log.Println("Upgrade http to webscoket successfully")
	conn := newConn(netConn)
	return conn, nil
}
